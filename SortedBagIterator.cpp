#include "SortedBagIterator.h"
#include "SortedBag.h"
#include <exception>
#include <iostream>

using namespace std;

SortedBagIterator::SortedBagIterator(const SortedBag& b) : bag(b) {
	//TODO - Implementation
	this->d_array.capacity =20;
	this->d_array.elements_number = 0;
	this->d_array.elements = new BSTNode*[this->d_array.capacity];

	BSTNode* aux_node = this->bag.tree.root;

	while (aux_node != nullptr)
	{
		if (this->d_array.capacity == this->d_array.elements_number)
		{
			resize(this->d_array);
			//std::cout << "resize" << std::endl;
		}
		this->d_array.elements[this->d_array.elements_number] = aux_node;
		this->d_array.elements_number++;
		aux_node = aux_node->left;
	}

	if (this->d_array.elements_number != 0)
		this->current_node = this->d_array.elements[this->d_array.elements_number - 1];
	else
		this->current_node = nullptr;
}
//Best case theta(1), Worst case theta(d_array.capacity) -> O(number_elements)- amortized

TComp SortedBagIterator::getCurrent() {
	//TODO - Implementation
	if (this->current_node == nullptr)
		throw exception();
	return this->current_node->information;
}
//theta(1)

bool SortedBagIterator::valid() {
	//TODO - Implementation
	return this->current_node != nullptr;
}
//theta(1)

void SortedBagIterator::next() {
	//TODO - Implementation

	if (this->current_node == nullptr)
		throw exception();

	BSTNode* aux_node = this->d_array.elements[this->d_array.elements_number - 1];
	this->d_array.elements_number--;

	if (aux_node->right != nullptr)
	{
		aux_node = aux_node->right;
		while (aux_node != nullptr)
		{
			if (this->d_array.capacity == this->d_array.elements_number)
			{
				resize(this->d_array);
				//std::cout << "resize" << std::endl;
			}
			this->d_array.elements[this->d_array.elements_number] = aux_node;
			this->d_array.elements_number++;
			aux_node = aux_node->left;
		}
	}
	if (this->d_array.elements_number != 0)
		this->current_node = this->d_array.elements[this->d_array.elements_number - 1];
	else
		this->current_node = nullptr;
}
//Best case theta(1), Worst case theta(d_array.capacity) -> O(number_elements)- amortized


void SortedBagIterator::first() {
	//TODO - Implementation
	this->d_array.capacity = 20;
	this->d_array.elements_number = 0;
	this->d_array.elements = new BSTNode * [this->d_array.capacity];

	BSTNode* aux_node = this->bag.tree.root;

	while (aux_node != nullptr)
	{
		if (this->d_array.capacity == this->d_array.elements_number)
		{
			resize(this->d_array);
			//std::cout << "resize" << std::endl;
		}
		this->d_array.elements[this->d_array.elements_number] = aux_node;
		this->d_array.elements_number++;
		aux_node = aux_node->left;
	}

	if (this->d_array.elements_number != 0)
		this->current_node = this->d_array.elements[this->d_array.elements_number - 1];
	else
		this->current_node = nullptr;

}
//Best case theta(1), Worst case theta(d_array.capacity) -> O(number_elements)- amortized


void resize(DynamicArray& d_array)
{
	//if (d_array.capacity == d_array.elements_number)	// we check if we need to resze the array

	d_array.capacity = d_array.capacity * 2;
	BSTNode** new_elements = new BSTNode*[d_array.capacity];

	for (int i = 0; i < d_array.elements_number; i++)
	{
		new_elements[i] = d_array.elements[i];
	}
	delete[] d_array.elements;
	d_array.elements = new_elements;
}
//theta(d_array.capacity)


