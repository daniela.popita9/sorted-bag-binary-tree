#pragma once
#include "SortedBag.h"

struct DynamicArray {
	BSTNode** elements;
	int capacity;
	int elements_number;
};

void resize(DynamicArray& d_array);

class SortedBag;

class SortedBagIterator
{
	friend class SortedBag;

private:
	const SortedBag& bag;
	SortedBagIterator(const SortedBag& b);
	DynamicArray d_array;
	BSTNode* current_node;
	//TODO - Representation

public:
	TComp getCurrent();
	bool valid();
	void next();
	void first();
};

