#include "SortedBag.h"
#include "SortedBagIterator.h"

SortedBag::SortedBag(Relation r) {
	//TODO - Implementation
	this->relation = r;
	this->number_elements = 0;
	this->tree.root = nullptr;
}
//theta(1)

void SortedBag::add(TComp e) {
	//TODO - Implementation

	BSTNode* new_node = new BSTNode; // create a new node with the given information
	new_node->information = e;
	new_node->left = nullptr;
	new_node->right = nullptr;

	if (this->tree.root == nullptr)
		this->tree.root = new_node;

	else {
		BSTNode* current_node = new BSTNode;
		BSTNode* parent_node = new BSTNode;
		
		parent_node = nullptr;
		current_node = this->tree.root;
		while (current_node != nullptr)
		{
			parent_node = current_node;
			if (this->relation(new_node->information, current_node->information))
				current_node = current_node->left;
			else
				current_node = current_node->right;
		}
		if (this->relation(e, parent_node->information))
			parent_node->left = new_node;
		else
			parent_node->right = new_node;
	}
	this->number_elements++;

}
//Best case theta(1), Worst case theta(number_elements) -> O(number_elements)



BSTNode* SortedBag::get_predecesor_parent(BSTNode* node)
{
	BSTNode* predecesor = new BSTNode;
	BSTNode* predecesor_parent = new BSTNode;

	predecesor_parent = node;
	predecesor = node->left;

	while (predecesor->right != nullptr)
	{
		predecesor_parent = predecesor;
		predecesor = predecesor->right;
	}
	return predecesor_parent;
}

//Best case theta(1), Worst case theta(number_elements) -> O(number_elements)

void SortedBag::recursive_remove(BSTNode* current_node, BSTNode* parent_node)
{
	if (current_node->left == nullptr && current_node->right == nullptr) // if the node is a leaf node
	{
		if (current_node == this->tree.root)
		{
			delete this->tree.root;
			this->tree.root = nullptr;
		}
		else
		{
			if (parent_node->left == current_node)
			{
				delete parent_node->left;
				parent_node->left = nullptr;
			}
			else
			{
				delete parent_node->right;
				parent_node->right = nullptr;
			}
		}
	}
	else if (current_node->left == nullptr) // if the node has only one right descendant
	{
		if (current_node == this->tree.root)
		{
			this->tree.root = this->tree.root->right;
		}
		else 
		{
			current_node->information = current_node->right->information;
			current_node->left = current_node->right->left;
			current_node->right = current_node->right->right;
		}
	}
	else if (current_node->right == nullptr) // if the node has only one left descendant
	{
		if (current_node == this->tree.root)
		{
			this->tree.root = this->tree.root->left;
		}
		else
		{
			current_node->information = current_node->left->information;
			current_node->right = current_node->left->right;
			current_node->left = current_node->left->left;
		}
	}
	else if (current_node->left != nullptr && current_node->right != nullptr) //if the node has 2 descendants 
	{
		BSTNode* predecesor = new BSTNode;
		BSTNode* predecesor_parent = new BSTNode;
		predecesor_parent = get_predecesor_parent(current_node);
		if(predecesor_parent== current_node)
		{
			current_node->information = predecesor_parent->left->information;
			recursive_remove(predecesor_parent->left, predecesor_parent);
		}
		else
		{
			current_node->information = predecesor_parent->right->information;
			recursive_remove(predecesor_parent->right, predecesor_parent);
		}
	}
}

bool SortedBag::remove(TComp e) {
	//TODO - Implementation
	
	BSTNode* current_node = new BSTNode;
	BSTNode* parent_node = new BSTNode;

	parent_node = nullptr;
	current_node = this->tree.root;
	while (current_node != nullptr && current_node->information != e)
	{
		parent_node = current_node;
		if (this->relation(e, current_node->information))
			current_node = current_node->left;
		else
			current_node = current_node->right;
	}
	if (current_node == nullptr)	//If we did not find the element
	{
		return false;
	}
	else if (current_node->information == e) // if we found the element
	{
		
		recursive_remove(current_node, parent_node);
		this->number_elements--;
		
		return true;
		
	}
	return false;
}
//Best case theta(1), Worst case theta(number_elements) -> O(number_elements)



bool SortedBag::search(TComp elem) const {
	//TODO - Implementation
	BSTNode* current_node = new BSTNode;

	current_node = this->tree.root;
	while (current_node != nullptr && current_node->information != elem)
	{
		if (this->relation(elem, current_node->information))
			current_node = current_node->left;
		else
			current_node = current_node->right;
	}
	if (current_node == nullptr)	//If we did not find the element
	{
		return false;
	}
	else
	{
		return true;
	}
}
//Best case theta(1), Worst case theta(number_elements) -> O(number_elements)


int SortedBag::nrOccurrences(TComp elem) const {
	//TODO - Implementation
	BSTNode* current_node = new BSTNode;
	int nr_occurences = 0;

	current_node = this->tree.root;
	while (current_node != nullptr)
	{
		if (current_node->information == elem) // if the current element has the value we are looking for
			nr_occurences++;
		
		if (this->relation(elem, current_node->information))
			current_node = current_node->left;
		else
			current_node = current_node->right;
	}
	return nr_occurences;
}
//Best case theta(1), Worst case theta(number_elements) -> O(number_elements)


int SortedBag::size() const {
	//TODO - Implementation
	return this->number_elements;
}
//theta(1)


bool SortedBag::isEmpty() const {
	//TODO - Implementation
	return (this->number_elements==0);
}
//theta(1)

SortedBagIterator SortedBag::iterator() const {
	return SortedBagIterator(*this);
}
//theta(1)

void SortedBag::delete_postorder_recursive(BSTNode* current_node)
{
	if (current_node != nullptr)
	{
		delete_postorder_recursive(current_node->left);
		delete_postorder_recursive(current_node->right);
		delete current_node;
	}
}
//Best case theta(1), Worst case theta(number_elements) -> O(number_elements)

SortedBag::~SortedBag() {
	//TODO - Implementation
	delete_postorder_recursive(this->tree.root);
}
//Best case theta(1), Worst case theta(number_elements) -> O(number_elements)
